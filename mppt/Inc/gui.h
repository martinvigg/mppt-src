/*
 * gui.h
 *
 *  Created on: Oct 24, 2019
 *      Author: Martin Viggiano
 */

#ifndef GUI_H_
#define GUI_H_

#include "display.h"
#include "ui.h"
#include "charger.h"

typedef struct {
	display_t* display;
	ui_t* ui;
	charge_t* charge;
} gui_t;

void gui_init(gui_t* gui, display_t* display, ui_t* ui, charge_t* charge);
void gui_update(gui_t* gui);

#endif /* GUI_H_ */
