/*
 * data_process.h
 *
 *  Created on: Oct 8, 2019
 *      Author: Martin Viggiano
 */

#ifndef INC_DATA_PROCESS_H_
#define INC_DATA_PROCESS_H_

#include "main.h"
#include "stm32f1xx_hal_adc.h"

typedef enum {
	ADC_PV_V_INDEX = 0,
	ADC_BT_V_INDEX,
	ADC_PV_I_INDEX,
	ADC_BT_I_INDEX,
	ADC_TEMP_INDEX
} adc_index_t;

typedef struct {
	int16_t offset;
	int16_t scale;
} calibration_t;

typedef struct {
	uint16_t bat_v;
	uint16_t bat_i;
	uint16_t pv_v;
	uint16_t pv_i;
	uint16_t tempC;
	/* Private */
	calibration_t calibration[5];
	uint16_t vref_num;
	uint16_t vref_den;
	uint16_t* raw_data;
	uint16_t  raw_data_length;
} data_t;

void DP_Init(data_t* data, uint16_t* raw_data, uint16_t raw_data_length);
void DP_Average_data(data_t* data);
void DP_Injected_data (data_t* data, ADC_HandleTypeDef* adc);
void DP_Vref_Calibration (data_t* data, ADC_HandleTypeDef* adc);



#endif /* INC_DATA_PROCESS_H_ */
