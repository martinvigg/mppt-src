/*
 * ui.h
 *
 *  Created on: Oct 8, 2019
 *      Author: Martin Viggiano
 */

#ifndef UI_H_
#define UI_H_

#include "display.h"

typedef enum {
	INPUT_INDEX_UP = 0,
	INPUT_INDEX_DOWN,
	INPUT_INDEX_BACK,
	INPUT_INDEX_ENTER
} ui_input_index_t;

typedef struct {
	uint8_t input_up;
	uint8_t input_down;
	uint8_t input_enter;
	uint8_t input_back;
	uint16_t input_debounce[4];
} ui_t;

void ui_init(ui_t* ui);
void ui_update_inputs(ui_t* ui);
void ui_clear_inputs(ui_t* ui);
void ui_clear_input(ui_t* ui, ui_input_index_t input);
void ui_set_input(ui_t* ui, ui_input_index_t index);
void ui_update(ui_t* ui);


#endif /* UI_H_ */
