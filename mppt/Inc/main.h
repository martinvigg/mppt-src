/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f1xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define LED_Pin GPIO_PIN_13
#define LED_GPIO_Port GPIOC
#define PV_V_Pin GPIO_PIN_0
#define PV_V_GPIO_Port GPIOA
#define BT_V_Pin GPIO_PIN_1
#define BT_V_GPIO_Port GPIOA
#define PV_I_Pin GPIO_PIN_2
#define PV_I_GPIO_Port GPIOA
#define BT_I_Pin GPIO_PIN_3
#define BT_I_GPIO_Port GPIOA
#define TEMP_Pin GPIO_PIN_4
#define TEMP_GPIO_Port GPIOA
#define VREF25_Pin GPIO_PIN_5
#define VREF25_GPIO_Port GPIOA
#define _10VENA_Pin GPIO_PIN_1
#define _10VENA_GPIO_Port GPIOB
#define UI1_Pin GPIO_PIN_12
#define UI1_GPIO_Port GPIOB
#define UI2_Pin GPIO_PIN_14
#define UI2_GPIO_Port GPIOB
#define ABFENA_Pin GPIO_PIN_15
#define ABFENA_GPIO_Port GPIOB
#define LCD_NSS_Pin GPIO_PIN_9
#define LCD_NSS_GPIO_Port GPIOA
#define LCD_DC_Pin GPIO_PIN_10
#define LCD_DC_GPIO_Port GPIOA
#define LCD_BL_Pin GPIO_PIN_11
#define LCD_BL_GPIO_Port GPIOA
#define LCD_RESET_Pin GPIO_PIN_12
#define LCD_RESET_GPIO_Port GPIOA
#define SD_NSS_Pin GPIO_PIN_15
#define SD_NSS_GPIO_Port GPIOA
#define SP1_SCK_Pin GPIO_PIN_3
#define SP1_SCK_GPIO_Port GPIOB
#define SPI1_MISO_Pin GPIO_PIN_4
#define SPI1_MISO_GPIO_Port GPIOB
#define SPI1_MOSI_Pin GPIO_PIN_5
#define SPI1_MOSI_GPIO_Port GPIOB
#define LED1_Pin GPIO_PIN_6
#define LED1_GPIO_Port GPIOB
#define LED2_Pin GPIO_PIN_7
#define LED2_GPIO_Port GPIOB
#define UI3_Pin GPIO_PIN_8
#define UI3_GPIO_Port GPIOB
#define UI4_Pin GPIO_PIN_9
#define UI4_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
