/*
 * logging.h
 *
 *  Created on: Nov 11, 2019
 *      Author: Martin Viggiano
 */

#ifndef LOGGING_H_
#define LOGGING_H_

#include "fatfs.h"
#include "charger.h"

typedef enum {
	LOG_OK,
	LOG_NOINIT,
	LOG_NOSD
} logging_status_t;


typedef struct {
	uint32_t bat_v;
	uint32_t bat_i;
	uint32_t pv_v;
	uint32_t pv_i;
	uint16_t samples;

	uint16_t tick_write;
	uint16_t period_write;

	data_t* data;
	charge_t* charge;
	logging_status_t status;
} logging_t;

void log_init(logging_t* logSD, data_t* data, charge_t* charge, uint16_t period);
void log_update(logging_t* logSD);


#endif /* LOGGING_H_ */
