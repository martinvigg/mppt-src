/*
 * param.h
 *
 *  Created on: Oct 8, 2019
 *      Author: Martin Viggiano
 */

#ifndef INC_PARAM_H_
#define INC_PARAM_H_

#define ADC_PV_V_INDEX 1
#define ADC_PV_I_INDEX 3
#define ADC_BT_V_INDEX 2
#define ADC_BT_I_INDEX 4



#endif /* INC_PARAM_H_ */
