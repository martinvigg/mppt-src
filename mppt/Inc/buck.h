/*
 * buck.h
 *
 *  Created on: Oct 8, 2019
 *      Author: Martin Viggiano
 */

#ifndef BUCK_H_
#define BUCK_H_

#include "main.h"
#include "data_process.h"

typedef enum {
	BUCK_MODE_OFF = 0,
	BUCK_MODE_STARTUP,
	BUCK_MODE_SYNC,
	BUCK_MODE_ASYNC
} buck_mode_t;

typedef enum {
	BUCK_ON = 0,
	BUCK_OFF
} buck_status_t;

typedef struct {
	buck_mode_t mode;
	uint8_t 	reg_out_V;
	uint8_t 	reg_out_C;
	uint8_t		reg_out_OV;
	uint8_t		reg_out_OC;
	uint8_t 	enable;
	uint16_t 	duty;
	/* Estas 4 podrian reemplazarse por locales en la funcion de update. */
	uint16_t 	v_pv;
	uint16_t 	i_pv;
	uint16_t 	v_bat;
	uint16_t 	i_bat;
	/* -  - */

	uint16_t tick_mode;

	// Output regulation target
	uint16_t 	v_pv_sp;
	uint16_t 	v_bat_sp;
	uint16_t 	i_bat_sp;

	// Timer variables.
	TIM_HandleTypeDef* _pwm_timer;
	uint16_t 	_pwm_channel;

	// Data process handle
	data_t* data;
} buck_t;

/**
 * This function initiates the buck structure.
 * The PWM timer handle and channel are given as arguments.
 * The rest of the variables should be initialized to default values.
 * The PWM timer is disabled as well as its PWM & PWMN channels.
 * @param buck
 * @param _pwm_timer
 * @param _pwm_channel
 */
void buck_init(buck_t* buck, TIM_HandleTypeDef* _pwm_timer, uint16_t _pwm_channel, data_t* data);

/**
 *
 * @param buck
 */
void buck_update(buck_t* buck);
void buck_enable(buck_t* buck, buck_status_t status);
void buck_set_solar_V(buck_t* buck, uint16_t voltage);
void buck_set_bat_V(buck_t* buck, uint16_t voltage);
void buck_set_bat_I(buck_t* buck, uint16_t current);
uint8_t buck_get_mppt_enable(buck_t* buck);
void _buck_update_pwm(buck_t* buck);


#endif /* BUCK_H_ */
