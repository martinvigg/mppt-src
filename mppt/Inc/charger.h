/*
 * charger.h
 *
 *  Created on: Nov 6, 2019
 *      Author: Martin Viggiano
 */

#ifndef CHARGER_H_
#define CHARGER_H_

#include "buck.h"

/* DEVICE LIMIT DEFINES */


/* MPPT PARAMETER DEFINE */
#define MPPT_I_LOW 200
#define MPPT_I_MED 500
#define MPPT_I_HIGH 1000
#define MPPT_DV_LOW 300
#define MPPT_DV_MED 200
#define MPPT_DV_HIGH 100

typedef enum {
	CHARGER_STATE_OFF = 0,
	CHARGER_STATE_STARTUP,
	CHARGER_STATE_IDLE,
	CHARGER_STATE_BULK,
	CHARGER_STATE_ABS,
	CHARGER_STATE_FLOAT,
	CHARGER_STATE_FAULT_OVP,
	CHARGER_STATE_FAULT_UVP,
	CHARGER_STATE_FAULT_TEMP,
	CHARGER_STATE_ERROR
} charge_state_t;

typedef struct {
	uint16_t v_float; /**< Float voltage */
	uint16_t v_abs; /**< Bulk/abs voltage */
	uint16_t i_bulk; /**< Bulk current */
	uint16_t i_abs; /**< Abs 2 float current */
	uint16_t v_uvp; /**< Battery min voltage */
	uint16_t v_ovp; /**< Battery max voltage */
	uint16_t v_float2bulk; /**< Bulk reconnect voltage */
	uint16_t timeout_abs_acum; /**< Daily accumulated abs time */
	uint16_t timeout_state_change; /**< State change timeout */
	uint16_t timeout_off; /**< Charger off timeout */
	int16_t temp_compensation; /**< Voltage temperature correction coefficient (mV/ºC) */
} charge_param_t;

typedef struct {
	const charge_param_t* charge_param; /**< Charge parameters */
	charge_state_t charge_state; /**< FSM state */
	buck_t* buck; /**< buck handle*/
	uint8_t charge_enable; /**< charger enable */
	uint8_t _10vena;
	uint8_t abfena;
	uint16_t mppt_sol_v_prev; /**< MPPT previous solar voltage.*/
	uint16_t mppt_sol_i_prev; /**< MPPT previous solar voltage.*/
	float mppt_sol_p_prev; /**< MPPT previous solar power.*/
	uint16_t tick_abs_acum; /**< Absorption time tick.*/
	uint16_t tick_state_change; /**< FSM state change tick.*/
	uint16_t tick_off; /**< Charger off tick.*/
	uint16_t tick_on; /**< Charger off tick.*/
} charge_t;

void charge_update(charge_t* charge);
void charge_mppt(charge_t* charge);
void charge_init(charge_t* charge, buck_t* buck);
void charge_set_param(charge_t* charge, const charge_param_t* param);
void charge_enable(charge_t* charge, uint8_t enable);

#endif /* CHARGER_H_ */
