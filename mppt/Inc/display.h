/*
 * display.h
 *
 *  Created on: Oct 8, 2019
 *      Author: Martin Viggiano
 */

#ifndef DISPLAY_H_
#define DISPLAY_H_

#include "u8g2.h"

typedef enum {
	DISPLAY_MODE_STARTUP = 0,
	DISPLAY_MODE_OVERVIEW,
	DISPLAY_MODE_OVERVIEW_OFF,
	DISPLAY_MODE_HOLA
} display_mode_t;

typedef struct {
	display_mode_t mode;
	char content[6][16];

	u8g2_t handle;
	u8x8_msg_cb byte_cb;
	u8x8_msg_cb gpio_cb;
} display_t;

void display_init(display_t* display, u8x8_msg_cb byte_cb, u8x8_msg_cb gpio_cb);
void display_prepare(display_t* display);
void display_update(display_t* display);
void display_set_mode(display_t* display, display_mode_t mode);
display_mode_t display_get_mode(display_t* display);

#endif /* DISPLAY_H_ */
