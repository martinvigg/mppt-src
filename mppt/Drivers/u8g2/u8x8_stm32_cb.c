/*
 * u8x8_stm32_cb.c
 *
 *  Created on: Oct 3, 2019
 *      Author: Martin Viggiano
 */

#include "u8x8.h"
#include "spi.h"
#include "gpio.h"

uint8_t u8x8_stm32_gpio_and_delay(u8x8_t *u8x8, uint8_t msg, uint8_t arg_int,
		void *arg_ptr);
uint8_t u8x8_byte_4wire_hw_spi(u8x8_t *u8x8, uint8_t msg, uint8_t arg_int,
		void *arg_ptr);

uint8_t u8x8_stm32_gpio_and_delay(u8x8_t *u8x8, uint8_t msg,  uint8_t arg_int,
		void *arg_ptr)
{
  switch (msg)
  {
  case U8X8_MSG_GPIO_AND_DELAY_INIT:
    HAL_Delay(1);
    break;
  case U8X8_MSG_DELAY_MILLI:
    HAL_Delay(arg_int);
    break;
  case U8X8_MSG_GPIO_DC:
    HAL_GPIO_WritePin(LCD_DC_GPIO_Port, LCD_DC_Pin, arg_int);
    break;
  case U8X8_MSG_GPIO_RESET:
    HAL_GPIO_WritePin(LCD_RESET_GPIO_Port, LCD_RESET_Pin, arg_int);
    break;
  case U8X8_MSG_GPIO_CS:
	HAL_GPIO_WritePin(LCD_NSS_GPIO_Port, LCD_NSS_Pin, arg_int);
    break;
  default:
	return 0;
	break;
  }
  return 1;
}

uint8_t u8x8_byte_4wire_hw_spi(u8x8_t *u8x8, uint8_t msg, uint8_t arg_int,
		void *arg_ptr)
{
  switch (msg)
  {
  case U8X8_MSG_BYTE_SEND:
    HAL_SPI_Transmit(&hspi1, (uint8_t *) arg_ptr, arg_int, 1000);
    break;
  case U8X8_MSG_BYTE_INIT:
    break;
  case U8X8_MSG_BYTE_SET_DC:
    HAL_GPIO_WritePin(LCD_DC_GPIO_Port, LCD_DC_Pin, arg_int);
    break;
  case U8X8_MSG_BYTE_START_TRANSFER:
	//HAL_GPIO_WritePin(LCD_NSS_GPIO_Port, LCD_NSS_Pin, u8x8->display_info->chip_enable_level);
	HAL_GPIO_WritePin(LCD_NSS_GPIO_Port, LCD_NSS_Pin, RESET);
    break;
  case U8X8_MSG_BYTE_END_TRANSFER:
	HAL_GPIO_WritePin(LCD_NSS_GPIO_Port, LCD_NSS_Pin, SET);
	break;
  default:
    return 0;
  }
  return 1;
}

//


