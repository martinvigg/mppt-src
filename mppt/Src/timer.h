/*
 * timer.h
 *
 *  Created on: Nov 8, 2019
 *      Author: Martin Viggiano
 */

#ifndef TIMER_H_
#define TIMER_H_

#include "main.h"

typedef struct {
	uint8_t flag;
	uint16_t period;
	uint16_t tick;
} timer_tick_t;

typedef struct {
	uint8_t ISR_flag;
	uint8_t ISR_period;
	timer_tick_t tick_100ms;
	timer_tick_t tick_250ms;
	timer_tick_t tick_1000ms;
} timer_t1;

void timer_init(timer_t1* timer, uint8_t ISR_period);
void timer_update(timer_t1* timer);
void timer_isr(timer_t1* timer);



#endif /* TIMER_H_ */
