/*
 * charger.c
 *
 *  Created on: Nov 6, 2019
 *      Author: Martin Viggiano
 */

#include "charger.h"
#include "buck.h"

/** Device limits (independent of charge_param) */
#define CHARGER_V_PV_MIN 10800
#define CHARGER_V_PV_MAX 39000
#define CHARGER_V_BAT_MIN 10000
#define CHARGER_V_BAT_MAX 32000
#define CHARGER_TEMP_MIN 50
#define CHARGER_TEMP_MAX 600
#define CHARGER_V_START_TH 1500

#define BUCK_V_PV (charge->buck->v_pv)
#define BUCK_I_PV (charge->buck->i_pv)
#define BUCK_V_BAT (charge->buck->v_bat)
#define BUCK_I_BAT (charge->buck->i_bat)
#define BUCK_TEMPC (charge->buck->data->tempC)

static void _charge_10V_enable(charge_t* charge, GPIO_PinState enable);
static void _charge_abf_enable(charge_t* charge, GPIO_PinState enable);
static uint16_t _charge_temp_compensate(charge_t*charge, uint16_t v);

/**
 * @brief Inits the charger module.
 * @param charge
 * @param buck
 */
void charge_init(charge_t* charge, buck_t* buck) {
	charge->buck = buck;
	charge->charge_state = CHARGER_STATE_OFF;
	charge->tick_state_change= 0;
	charge->tick_abs_acum = 0;
	charge->tick_off = 0;
	charge->tick_on = 0;
	charge->charge_enable = 0;

	_charge_10V_enable(charge, GPIO_PIN_RESET);
	_charge_abf_enable(charge, GPIO_PIN_RESET);
}

/**
 * @brief Sets charge parameters.
 * @param charge
 * @param param
 */
void charge_set_param(charge_t* charge, const charge_param_t* param) {
	charge->charge_param = param;
}

/**
 * @brief Update FSM (1 second).
 * @param charge
 */
void charge_update(charge_t* charge) {
	// If charger not enabled, exit.
	if (charge->charge_enable == 0) {
		return;
	}

	// Exit & fault conditions

	/** Solar input limits (device). */
	if (BUCK_V_PV < CHARGER_V_PV_MIN || BUCK_V_PV > CHARGER_V_PV_MAX) {
		buck_enable(charge->buck, BUCK_OFF);
		_charge_10V_enable(charge, GPIO_PIN_RESET);
		_charge_abf_enable(charge, GPIO_PIN_RESET);
		charge->charge_state = CHARGER_STATE_OFF;
		return;
	}

	/** Battery input limit (device). */
	if ( BUCK_V_BAT < CHARGER_V_BAT_MIN || BUCK_V_BAT > CHARGER_V_BAT_MAX) {
		buck_enable(charge->buck, BUCK_OFF);
		_charge_10V_enable(charge, GPIO_PIN_RESET);
		_charge_abf_enable(charge, GPIO_PIN_RESET);
		charge->charge_state = CHARGER_STATE_ERROR;
		return;
	}

	/** Battery input limit (charge param). */
	if (BUCK_V_BAT < charge->charge_param->v_uvp) {
		buck_enable(charge->buck, BUCK_OFF);
		_charge_10V_enable(charge, GPIO_PIN_RESET);
		_charge_abf_enable(charge, GPIO_PIN_RESET);
		charge->charge_state = CHARGER_STATE_FAULT_UVP;
		return;
	}

	if (BUCK_V_BAT > charge->charge_param->v_ovp) {
		buck_enable(charge->buck, BUCK_OFF);
		_charge_10V_enable(charge, GPIO_PIN_RESET);
		_charge_abf_enable(charge, GPIO_PIN_RESET);
		charge->charge_state = CHARGER_STATE_FAULT_OVP;
		return;
	}

	/** Temperature limit (device). */
	if (BUCK_TEMPC < CHARGER_TEMP_MIN || BUCK_TEMPC > CHARGER_TEMP_MAX) {
		buck_enable(charge->buck, BUCK_OFF);
		_charge_10V_enable(charge, GPIO_PIN_RESET);
		_charge_abf_enable(charge, GPIO_PIN_RESET);
		charge->charge_state = CHARGER_STATE_FAULT_TEMP;
		return;
	}

	/** Low voltage shut-down (device). */
	if (charge->buck->v_pv < (charge->buck->v_bat + CHARGER_V_START_TH)) {
		buck_enable(charge->buck, BUCK_OFF);
		_charge_10V_enable(charge, GPIO_PIN_RESET);
		_charge_abf_enable(charge, GPIO_PIN_RESET);
		charge->charge_state = CHARGER_STATE_OFF;
		return;
	}

	// FSM state transitions
	switch (charge->charge_state) {
	case CHARGER_STATE_OFF:
		if (charge->buck->v_pv >= (charge->buck->v_bat + CHARGER_V_START_TH)) {
			if (++(charge->tick_on) >= charge->charge_param->timeout_state_change) {
				/* Reset ticks*/
				charge->tick_abs_acum = 0;
				charge->tick_on = 0;
				charge->tick_off = 0;
				charge->tick_state_change = 0;

				_charge_10V_enable(charge, GPIO_PIN_SET);
				_charge_abf_enable(charge, GPIO_PIN_SET);
				charge->charge_state = CHARGER_STATE_STARTUP;
			}
		} else {
			charge->tick_on = 0;
		}
		break;
	case CHARGER_STATE_STARTUP:
		if (charge->buck->v_bat >= charge->charge_param->v_float2bulk) {
			buck_set_bat_I(charge->buck, charge->charge_param->i_bulk);
			buck_set_bat_V(charge->buck, charge->charge_param->v_float);
			buck_set_solar_V(charge->buck, charge->buck->v_pv);
			buck_enable(charge->buck, BUCK_ON);
			charge->mppt_sol_p_prev = 0;
			charge->mppt_sol_v_prev = charge->buck->v_pv;
			charge->tick_state_change = 0;
			charge->charge_state = CHARGER_STATE_FLOAT;
		} else {
			buck_set_bat_I(charge->buck, charge->charge_param->i_bulk);
			buck_set_bat_V(charge->buck, charge->charge_param->v_abs);
			buck_set_solar_V(charge->buck, charge->buck->v_pv);
			buck_enable(charge->buck, BUCK_ON);
			charge->mppt_sol_p_prev = 0;
			charge->mppt_sol_v_prev = charge->buck->v_pv;
			charge->tick_state_change = 0;
			charge->charge_state = CHARGER_STATE_BULK;
		}
		break;

	case CHARGER_STATE_BULK:
		if (charge->buck->reg_out_V) {
			if (++(charge->tick_state_change) >= charge->charge_param->timeout_state_change) {
			charge->tick_state_change = 0;
			charge->charge_state = CHARGER_STATE_ABS;
			}
		} else {
			charge->tick_state_change = 0;
		}
		break;
	case CHARGER_STATE_ABS:
		if (charge->buck->reg_out_V && BUCK_I_BAT < charge->charge_param->i_abs) {
			if (++(charge->tick_state_change) >= charge->charge_param->timeout_state_change) {
				buck_set_bat_V(charge->buck, charge->charge_param->v_float);
				charge->tick_state_change = 0;
				charge->charge_state = CHARGER_STATE_FLOAT;
			}
		} else {
			charge->tick_state_change = 0;
		}

		if (++(charge->tick_abs_acum) >=  charge->charge_param->timeout_abs_acum) {
			buck_set_bat_V(charge->buck, charge->charge_param->v_float);
			charge->tick_state_change = 0;
			charge->charge_state = CHARGER_STATE_FLOAT;
		}
		break;
	case CHARGER_STATE_FLOAT:
		if (BUCK_V_BAT < charge->charge_param->v_float2bulk) {
			if (++(charge->tick_state_change) >= charge->charge_param->timeout_state_change) {
				buck_set_bat_I(charge->buck, charge->charge_param->i_bulk);
				buck_set_bat_V(charge->buck, charge->charge_param->v_abs);
				charge->tick_state_change = 0;
				charge->charge_state = CHARGER_STATE_BULK;
			}
		} else {
			charge->tick_state_change = 0;
		}
		break;
	default:
		buck_enable(charge->buck, BUCK_OFF);
		charge->charge_state = CHARGER_STATE_OFF;
		break;
	}
}


/**
 * @brief Update MPPT.
 * @param charge
 */
void charge_mppt(charge_t* charge) {
	// If charger not enabled, exit.
	if (charge->charge_enable == 0) {
		return;
	}

	if (!(charge->charge_state == CHARGER_STATE_BULK || charge->charge_state == CHARGER_STATE_ABS || charge->charge_state == CHARGER_STATE_FLOAT) ) {
		return;
	}

	if (buck_get_mppt_enable(charge->buck)) {
		float mppt_p;
		uint16_t mppt_v, mppt_i;
		mppt_v = charge->buck->v_pv;
		mppt_i = charge->buck->i_pv;
		mppt_p = (float)mppt_v / 1000.0 * (float)mppt_i / 1000.0;

		uint8_t dirV, dirI;
		dirV = mppt_v > charge->mppt_sol_v_prev;
		dirI = mppt_i > charge->mppt_sol_i_prev;

		uint16_t v_step;

		if (mppt_i < MPPT_I_LOW) {
			v_step = MPPT_DV_LOW;
		} else if (mppt_i < MPPT_I_MED) {
			v_step = MPPT_DV_MED;
		} else {
			v_step = MPPT_DV_HIGH;
		}

		if (mppt_p > charge->mppt_sol_p_prev) {
			if (dirV) {
				if (dirI) {
					charge->mppt_sol_v_prev -= v_step;
					if (charge->mppt_sol_v_prev < CHARGER_V_PV_MIN) {
						charge->mppt_sol_v_prev = CHARGER_V_PV_MIN;
					}
					buck_set_solar_V(charge->buck, charge->mppt_sol_v_prev);
				} else {
					charge->mppt_sol_v_prev += v_step;
					if (charge->mppt_sol_v_prev > CHARGER_V_PV_MAX) {
						charge->mppt_sol_v_prev = CHARGER_V_PV_MAX;
					}
					buck_set_solar_V(charge->buck, charge->mppt_sol_v_prev);
				}
			} else {
				charge->mppt_sol_v_prev -= v_step;
				if (charge->mppt_sol_v_prev < CHARGER_V_PV_MIN) {
					charge->mppt_sol_v_prev = CHARGER_V_PV_MIN;
				}
				buck_set_solar_V(charge->buck, charge->mppt_sol_v_prev);
			}
		} else {
			if (dirV) {
				charge->mppt_sol_v_prev -= v_step;
				if (charge->mppt_sol_v_prev < CHARGER_V_PV_MIN) {
					charge->mppt_sol_v_prev = CHARGER_V_PV_MIN;
				}
				buck_set_solar_V(charge->buck, charge->mppt_sol_v_prev);

			} else {
				charge->mppt_sol_v_prev += v_step;
				if (charge->mppt_sol_v_prev > CHARGER_V_PV_MAX) {
					charge->mppt_sol_v_prev = CHARGER_V_PV_MAX;
				}
				buck_set_solar_V(charge->buck, charge->mppt_sol_v_prev);
			}
		}

		charge->mppt_sol_p_prev = mppt_p;
		charge->mppt_sol_i_prev = mppt_i;
		charge->mppt_sol_v_prev = mppt_v;

	} else {
		// Track solar voltage.
		charge->mppt_sol_v_prev = charge->buck->v_pv;
		charge->mppt_sol_p_prev = charge->buck->v_pv / 1000 * charge->buck->i_pv / 1000;
	}
}

/**
 * @brief Enable charger.
 * @param charge
 * @param enable
 */
void charge_enable(charge_t* charge, uint8_t enable) {
	charge->charge_enable = enable;
}

/**
 * @brief Enable auxiliary 10V supply.
 * @param charge
 * @param enable
 */
static void _charge_10V_enable(charge_t* charge, GPIO_PinState enable) {
	charge->_10vena = enable;
	HAL_GPIO_WritePin(_10VENA_GPIO_Port, _10VENA_Pin, !enable);
}

/**
 *
 * @param charge
 * @param enable
 */
static void _charge_abf_enable(charge_t* charge, GPIO_PinState enable) {
	charge->abfena = enable;
	HAL_GPIO_WritePin(ABFENA_GPIO_Port, ABFENA_Pin, enable);
}

static uint16_t _charge_temp_compensate(charge_t* charge, uint16_t v) {
	return (uint16_t) v * charge->charge_param->temp_compensation * (BUCK_TEMPC - 250) / 10.0;
}
