/*
 * buck.c
 *
 *  Created on: Oct 8, 2019
 *      Author: Martin Viggiano
 */

#include "buck.h"

#define BUCK_V_HYST 75
#define BUCK_V_DEADBAND (15)
#define BUCK_I_HYST 50
#define BUCK_I_DEADBAND (0)
#define BUCK_DUTY_MAX_RANGE 1000
#define BUCK_DUTY_MAX (0.9 * BUCK_DUTY_MAX_RANGE)
#define BUCK_DUTY_MIN_SYNC (0.6* BUCK_DUTY_MAX_RANGE)
#define BUCK_DUTY_MIN_ASYNC (0.2 * BUCK_DUTY_MAX_RANGE)
#define BUCK_DUTY_MAX_ASYNC (0.9 * BUCK_DUTY_MAX_RANGE)
#define BUCK_DUTY_MAX_SYNC BUCK_DUTY_MAX
#define BUCK_DUTY_TH (0.025 * BUCK_DUTY_MAX_RANGE)
#define BUCK_I_ASYNC 300
#define BUCK_I_SYNC 500
#define BUCK_DUTY_STEP 1
#define BUCK_TICK_MODE 300
#define BUCK_TICK_CC_CV 100

/* LIMIT DEFINES */
#define BUCK_BAT_V_MAX 32000
#define BUCK_BAT_I_MAX 5000
#define BUCK_SOL_V_MAX 39000
#define BUCK_BAT_V_MIN 10000
#define BUCK_BAT_I_MIN 0
#define BUCK_SOL_V_MIN 10800

static void _buck_update_values(buck_t* buck);
static void _buck_get_startup_pwm(buck_t* buck);
static void _buck_sync2async(buck_t* buck);
static void _buck_async2sync(buck_t* buck);
static void _buck_startup(buck_t* buck);
static void _buck_shutdown(buck_t* buck);

void buck_init(buck_t* buck, TIM_HandleTypeDef* _pwm_timer, uint16_t _pwm_channel, data_t* data)
{
	buck->mode = BUCK_MODE_OFF;
	buck->enable = BUCK_OFF;
	buck->v_pv_sp = 0;
	buck->v_bat_sp = 0;
	buck->i_bat_sp = 0;
	buck->duty = BUCK_DUTY_MAX;

	buck->data = data;

	buck->_pwm_timer = _pwm_timer;
	buck->_pwm_channel = _pwm_channel;

	__HAL_TIM_DISABLE(buck->_pwm_timer);
	HAL_TIM_PWM_Stop(buck->_pwm_timer, buck->_pwm_channel);
	HAL_TIMEx_PWMN_Stop(buck->_pwm_timer, buck->_pwm_channel);
}

void buck_update(buck_t *buck) {
	_buck_update_values(buck);
	switch (buck->mode) {
	case BUCK_MODE_OFF:
		if (buck->enable == 1) {
			buck->mode = BUCK_MODE_STARTUP;
		}
		break;
	case BUCK_MODE_STARTUP:
		if (buck->enable == 1) {
			_buck_startup(buck);
			buck->mode = BUCK_MODE_ASYNC;
		} else {
			_buck_shutdown(buck);
			buck->mode = BUCK_MODE_OFF;
		}
		break;
	case BUCK_MODE_SYNC:
		if (buck->enable == 1) {
			//_buck_update_values(buck);
			//reg_out_0V: output over-voltage flag
			buck->reg_out_OV = buck->v_bat > buck->v_bat_sp;
			//reg_out_0C: output over-current flag
			buck->reg_out_OC = buck->i_bat > buck->i_bat_sp;
			//reg_out_V: output voltage inside target flag
			buck->reg_out_V = (!buck->reg_out_OV)
									&& (buck->v_bat >= (buck->v_bat_sp - BUCK_V_HYST));
			//reg_out_C: output current inside target flag
			buck->reg_out_C = (!buck->reg_out_OC)
									&& (buck->i_bat >= (buck->i_bat_sp - BUCK_I_HYST));

			if (buck->v_pv < buck->v_pv_sp || buck->reg_out_OC
					|| buck->reg_out_OV) {
				/*
				 * Decrease duty cycle in order to raise solar_V or to
				 * get out of OV or OC conditions.
				 */
				if (buck->duty > (BUCK_DUTY_MIN_SYNC + BUCK_DUTY_STEP)) {
					buck->duty -= BUCK_DUTY_STEP;
					_buck_update_pwm(buck);
				} else {
//#warning "El modo asyncronico queda en DCM y tira unas formas de onda del futuro, esta bien?"
					_buck_sync2async(buck);
					buck->tick_mode = 0;
					buck->mode = BUCK_MODE_ASYNC;
				}
			} else if (buck->v_pv >= buck->v_pv_sp
					&& !(buck->reg_out_V)) {
				/*
				 * Increase duty cycle in order to reduce solar_v if not
				 * in voltage or current target condition.
				 */
				if (buck->duty < (BUCK_DUTY_MAX - BUCK_DUTY_STEP)) {
					buck->duty += BUCK_DUTY_STEP;
					_buck_update_pwm(buck);
				}
			}

			// Switch to async mode if current is low. This prevents sync buck becoming boost when
			// panel is disconnected.
			if ((buck->mode == BUCK_MODE_SYNC)
					&& (buck->i_bat < BUCK_I_ASYNC)) {
				if (++(buck->tick_mode) >= BUCK_TICK_MODE) {
					_buck_sync2async(buck);
					buck->tick_mode = 0;
					buck->mode = BUCK_MODE_ASYNC;
				}
			} else {
				buck->tick_mode = 0;
			}
		} else {
			_buck_shutdown(buck);
			buck->mode = BUCK_MODE_OFF;
		}
		break;
	case BUCK_MODE_ASYNC:
		if (buck->enable == 1) {
			//_buck_update_values(buck);
			//reg_out_0V: output over-voltage flag
			buck->reg_out_OV = buck->v_bat > buck->v_bat_sp;
			//reg_out_0C: output over-current flag
			buck->reg_out_OC = buck->i_bat > buck->i_bat_sp;
			//reg_out_V: output voltage inside target flag
			buck->reg_out_V = (!buck->reg_out_OV)
									&& (buck->v_bat >= (buck->v_bat_sp - BUCK_V_HYST));
			//reg_out_C: output current inside target flag
			buck->reg_out_C = (!buck->reg_out_OC)
									&& (buck->i_bat >= (buck->i_bat_sp - BUCK_I_HYST));

			if (buck->v_pv < buck->v_pv_sp || buck->reg_out_OC
					|| buck->reg_out_OV) {
				/*
				 * Decrease duty cycle in order to raise solar_V or to
				 * get out of OV or OC conditions.
				 */
				if (buck->duty > (BUCK_DUTY_MIN_ASYNC + BUCK_DUTY_STEP)) {
					buck->duty -= BUCK_DUTY_STEP;
					_buck_update_pwm(buck);
				}
			} else if (buck->v_pv > buck->v_pv_sp
					&& !(buck->reg_out_V)) {
				/*
				 * Increase duty cycle in order to reduce solar_v if not
				 * in voltage or current target condition.
				 */
				if (buck->duty < (BUCK_DUTY_MAX - BUCK_DUTY_STEP)) {
					buck->duty += BUCK_DUTY_STEP;
					_buck_update_pwm(buck);
				} else if (0) {
					_buck_async2sync(buck);
					buck->tick_mode = 0;
					buck->mode = BUCK_MODE_SYNC;
				}
			}

			// Switch to sync mode if current is high and duty cycle is big enough.
			// Buck must be operating in CCM to switch.
			if ((buck->mode == BUCK_MODE_ASYNC)
					&& (buck->i_bat > BUCK_I_SYNC)
					&& (buck->duty > (BUCK_DUTY_MIN_SYNC + BUCK_DUTY_TH))) {
				if (++(buck->tick_mode) >= BUCK_TICK_MODE) {
				_buck_async2sync(buck);
				buck->tick_mode = 0;
				buck->mode = BUCK_MODE_SYNC;
				}
			} else {
				buck->tick_mode = 0;
			}
		} else {
			_buck_shutdown(buck);
			buck->mode = BUCK_MODE_OFF;
		}
		break;
	default:
		_buck_shutdown(buck);
		buck->mode = BUCK_MODE_OFF;
		break;
	}
}


/**
 * @brief This funcion updates the PWM timer's compare register.
 * @param buck
 */
void _buck_update_pwm(buck_t* buck)
{
	__HAL_TIM_SET_COMPARE(buck->_pwm_timer, buck->_pwm_channel, buck->duty);
}

/**
 * @brief This function sets or resets the buck's enable flag.
 * @param buck
 * @param status
 */
void buck_enable(buck_t* buck, buck_status_t status)
{
	switch (buck->mode) {
	case BUCK_MODE_OFF:
		if (status == BUCK_ON) {
			buck->enable = 1;
		} else {
			buck->enable = 0;
		}
		break;
	case BUCK_MODE_SYNC:
		if (status == BUCK_OFF) {
			buck->enable = 0;
		}
		break;
	case BUCK_MODE_ASYNC:
		if (status == BUCK_OFF) {
			buck->enable = 0;
		}
		break;
	case BUCK_MODE_STARTUP:
	default:
		break;
	}
}

static void _buck_startup(buck_t* buck)
{
	/**
	 * This is critical code so interrupts are disabled.
	 *
	 * The function _buck_get_startup_pwm calculates a neutral duty cycle
	 * that is equal to the relationship between input and output voltages
	 * in order to reduce the startup inrush current of the buck converter.
	 *
	 * The counter is set to zero, PWM is enabled for both channels and then
	 * the timer is enabled. The startup procedure runs for 16 timer cycles
	 * and then switches to asyncronous mode. TODO: implement buck async mode.
	 *
	 * Finally, interrupts are enabled.
	 */
	__disable_irq();
	_buck_update_values(buck);
	_buck_get_startup_pwm(buck);
	_buck_update_pwm(buck);
	__HAL_TIM_SET_COUNTER(buck->_pwm_timer, 0);
	__HAL_TIM_ENABLE(buck->_pwm_timer);
	HAL_TIM_PWM_Start(buck->_pwm_timer, buck->_pwm_channel);
	HAL_TIMEx_PWMN_Start(buck->_pwm_timer, buck->_pwm_channel);

	uint8_t period_counter = 0;

	while(period_counter<16) {
		if (__HAL_TIM_GET_FLAG(buck->_pwm_timer, TIM_FLAG_UPDATE)) {
			__HAL_TIM_CLEAR_FLAG(buck->_pwm_timer, TIM_FLAG_UPDATE);
			++period_counter;
		}
	}

	_buck_sync2async(buck);
	__enable_irq();
}

/**
 * @brief This function disables PWMN channel to run in async mode.
 * @param buck
 */
static void _buck_sync2async(buck_t* buck) {
	__disable_irq();
	//while(!__HAL_TIM_GET_FLAG(buck->_pwm_timer, TIM_FLAG_UPDATE)) {};
	HAL_TIMEx_PWMN_Stop(buck->_pwm_timer, buck->_pwm_channel);
	//__HAL_TIM_CLEAR_FLAG(buck->_pwm_timer, TIM_FLAG_UPDATE);
	__enable_irq();
}

/**
 * @brief This function enables PWMN channel to run in sync mode.
 * @param buck
 */
static void _buck_async2sync(buck_t* buck) {
	__disable_irq();
	//while(!__HAL_TIM_GET_FLAG(buck->_pwm_timer, TIM_FLAG_UPDATE)) {};
	HAL_TIMEx_PWMN_Start(buck->_pwm_timer, buck->_pwm_channel);
	//__HAL_TIM_CLEAR_FLAG(buck->_pwm_timer, TIM_FLAG_UPDATE);
	__enable_irq();
}


/**
 *
 * @param buck
 */
static void _buck_shutdown(buck_t* buck)
{
	/**
	 * This is critical code so interrupts are disabled.
	 *
	 * The function stops the PWM & the PWMN channels and then disables the
	 * PWM timer.
	 *
	 * Finally, interrupts are enabled.
	 */
	__disable_irq();
	HAL_TIM_PWM_Stop(buck->_pwm_timer, buck->_pwm_channel);
	HAL_TIMEx_PWMN_Stop(buck->_pwm_timer, buck->_pwm_channel);
	__HAL_TIM_ENABLE(buck->_pwm_timer);
	__enable_irq();
}

/**
 * This function sets the solar target voltage.
 * @param buck
 * @param voltage
 */
void buck_set_solar_V(buck_t* buck, uint16_t voltage)
{
	if (voltage < BUCK_SOL_V_MAX && voltage > BUCK_SOL_V_MIN) {
		buck->v_pv_sp = voltage;
	}
}

/**
 * This function sets the battery target/max voltage.
 * @param buck
 * @param voltage
 */
void buck_set_bat_V(buck_t* buck, uint16_t voltage)
{
	if (voltage < BUCK_BAT_V_MAX && voltage > BUCK_BAT_V_MIN) {
		buck->v_bat_sp = voltage;
	}
}

/**
 * This function sets the battery target/max current.
 * @param buck
 * @param current
 */
void buck_set_bat_I(buck_t* buck, uint16_t current)
{
	if (current < BUCK_BAT_I_MAX && current > BUCK_BAT_I_MIN) {
		buck->i_bat_sp = current;
	}
}

/**
 * This function calculates a neutral duty cycle that is equal to the
 * relationship between input and output voltages in order to reduce
 * the startup in-rush current of the buck converter.
 * @param buck
 */
static void _buck_get_startup_pwm(buck_t* buck)
{
	uint32_t duty = 0;
	duty = buck->v_bat*BUCK_DUTY_MAX/buck->v_pv;
	if (duty > BUCK_DUTY_MAX) {
		duty = BUCK_DUTY_MAX;
	} else if (duty < BUCK_DUTY_MIN_SYNC) {
		duty = BUCK_DUTY_MIN_SYNC;
	}
	buck->duty = duty;
}

/**
 * This function retrieves input and output voltages and currents and
 * updates the values in the buck structure.
 * @param buck
 */
static void _buck_update_values(buck_t* buck) {
	buck->v_pv = buck->data->pv_v;
	buck->i_pv = buck->data->pv_i;
	buck->v_bat = buck->data->bat_v;
	buck->i_bat = buck->data->bat_i;
}

/**
 * This function evaluates if buck is regulating its output. If not, MPPT can be enabled.
 * @param buck
 * @return
 */
uint8_t buck_get_mppt_enable(buck_t *buck) {
	return !(buck->reg_out_C || buck->reg_out_OC || buck->reg_out_V
			|| buck->reg_out_OV);
}


