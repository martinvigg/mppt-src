/*
 * logging.c
 *
 *  Created on: Nov 11, 2019
 *      Author: Martin Viggiano
 */

#include "logging.h"
#include "charger.h"
#include "fatfs.h"
#include "buck.h"
#include "rtc.h"
#include <string.h>
#include <stdio.h>

extern RTC_HandleTypeDef hrtc;
FRESULT result;

static void log_clear_accum(logging_t* logSD);
static void log_add_data(logging_t* logSD);
static void log_write_data(logging_t* logSD);
static void log_sd_mount(logging_t* logSD);
static char log_get_mode(logging_t* logSD);
static char log_get_mppt(logging_t *logSD);
static float log_uint32_2_float(uint32_t* v, uint16_t* s);
static void log_write_date(char* str);

void log_init(logging_t* logSD, data_t* data, charge_t* charge, uint16_t period) {
	logSD->data = data;
	logSD->charge = charge;
	logSD->period_write = period;
	logSD->tick_write = 0;
	logSD->status = LOG_NOINIT;

	log_clear_accum(logSD);
	log_sd_mount(logSD);
}

void log_update(logging_t* logSD) {
	switch (logSD->status) {
	case LOG_OK:
		log_add_data(logSD);
		logSD->tick_write += 1;
		if (logSD->tick_write >= logSD->period_write) {
			logSD->tick_write = 0;

			log_write_data(logSD);
			//log_clear_accum(logSD);
			logSD->bat_i = 0;
			logSD->bat_v = 0;
			logSD->pv_i  = 0;
			logSD->pv_v  = 0;
			logSD->samples = 0;
			//logSD->tick_write = 0;
		}
		break;
	default:
		break;
	}
}

static void log_clear_accum(logging_t* logSD) {
	logSD->bat_i = 0;
	logSD->bat_v = 0;
	logSD->pv_i  = 0;
	logSD->pv_v  = 0;
	logSD->samples = 0;
	logSD->tick_write = 0;
}

static void log_add_data(logging_t* logSD) {
	logSD->bat_v += logSD->data->bat_v;
	logSD->bat_i += logSD->data->bat_i;
	logSD->pv_v += logSD->data->pv_v;
	logSD->pv_i += logSD->data->pv_i;
	++(logSD->samples);
}

static void log_write_data(logging_t* logSD) {
	result = f_open(&USERFile, "log.csv", FA_READ | FA_WRITE | FA_OPEN_ALWAYS);
	if (result == FR_OK) {
		result = f_lseek(&USERFile, f_size(&USERFile));
		if (result == FR_OK) {
			char dataStr[64];
			char dateStr[18];
			log_write_date(dateStr);
			if (logSD->charge->charge_state == CHARGER_STATE_OFF) {
				sprintf(dataStr, "%s,%.2f,0,%.2f,0,%c,%c\n",
						dateStr,
						log_uint32_2_float(&(logSD->pv_v), &(logSD->samples)),
						log_uint32_2_float(&(logSD->bat_v), &(logSD->samples)),
						log_get_mode(logSD),
						log_get_mppt(logSD));
			} else {
				sprintf(dataStr, "%s,%.2f,%.2f,%.2f,%.2f,%c,%c\n",
						dateStr,
						log_uint32_2_float(&(logSD->pv_v), &(logSD->samples)),
						log_uint32_2_float(&(logSD->pv_i), &(logSD->samples)),
						log_uint32_2_float(&(logSD->bat_v), &(logSD->samples)),
						log_uint32_2_float(&(logSD->bat_i), &(logSD->samples)),
						log_get_mode(logSD),
						log_get_mppt(logSD));
			}
			UINT bwritten;
			result = f_write(&USERFile, dataStr, strlen(dataStr), &bwritten);
			if (result == FR_OK) {
				f_close(&USERFile);
			} else {
				logSD->status = LOG_NOSD;
			}
		}else {
			logSD->status = LOG_NOSD;
		}
	} else {
		logSD->status = LOG_NOSD;
	}
}

static void log_sd_mount(logging_t* logSD) {
	if (logSD->status == LOG_NOSD) {
		f_mount(0, "", 0);
	}
	result = f_mount(&USERFatFS, "", 1);
	if (result == FR_OK) {
		logSD->status = LOG_OK;
	} else {
		logSD->status = LOG_NOSD;
	}
}

static float log_uint32_2_float(uint32_t* v,uint16_t* s) {
	return (float) (((*v) / (float) (*s)) / 1000.0);
}

static char log_get_mode(logging_t* logSD) {
	switch (logSD->charge->charge_state ) {
		case CHARGER_STATE_OFF:
			return 'O';
			break;
		case CHARGER_STATE_BULK:
			return 'B';
			break;
		case CHARGER_STATE_ABS:
			return 'A';
			break;
		case CHARGER_STATE_FLOAT:
			return 'F';
			break;
		default:
			return 'D';
			break;
	}
}

static void log_write_date(char* str) {
	RTC_TimeTypeDef t;
	RTC_DateTypeDef d;
	HAL_RTC_GetTime(&hrtc, &t, RTC_FORMAT_BIN);
	HAL_RTC_GetDate(&hrtc, &d, RTC_FORMAT_BIN);

	sprintf(str, "%.2d-%.2d-%4d %.2d:%.2d:%.2d", d.Date, d.Month, 2000+d.Year, t.Hours,
			t.Minutes, t.Seconds);
}

static char log_get_mppt(logging_t *logSD) {
	if (logSD->charge->charge_state == CHARGER_STATE_BULK
			|| logSD->charge->charge_state == CHARGER_STATE_ABS
			|| logSD->charge->charge_state == CHARGER_STATE_FLOAT) {
		if (buck_get_mppt_enable(logSD->charge->buck)) {
			return 'M';
		}
	}
	return 'O';
}

