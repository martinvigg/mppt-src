/*
 * charge_param.c
 *
 *  Created on: Nov 8, 2019
 *      Author: Martin Viggiano
 */

#include "charger.h"

// V_FLOAT, V_ABS, I_BULK, I_ABS, V_UVP, V_OVP, V_F2B, T_ABS_ACCUM, T_STATE_CHANGE, T_OFF, TEMP COMP
const charge_param_t lead_acid_12V_600mA = { 13800, 14400, 800, 160, 10800, 15000, 12750, 60000, 5, 5 , -30};
