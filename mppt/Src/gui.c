/*
 * gui.c
 *
 *  Created on: Oct 24, 2019
 *      Author: Martin Viggiano
 */

#include "gui.h"
#include "display.h"
#include "ui.h"
#include "charger.h"

#define INPUT_DEBOUNCE 20

void gui_init(gui_t* gui, display_t* display, ui_t* ui, charge_t* charge) {
	gui->display = display;
	gui->ui = ui;
	gui->charge = charge;
}

void gui_update(gui_t* gui) {
	switch (display_get_mode(gui->display)) {
	case DISPLAY_MODE_OVERVIEW:
	case DISPLAY_MODE_OVERVIEW_OFF:
		if (gui->charge->charge_state == CHARGER_STATE_OFF) {
			display_set_mode(gui->display, DISPLAY_MODE_OVERVIEW_OFF);
		} else {
			display_set_mode(gui->display, DISPLAY_MODE_OVERVIEW);
		}

		if (gui->ui->input_enter && gui->ui->input_debounce[INPUT_INDEX_ENTER] > INPUT_DEBOUNCE) {
			display_set_mode(gui->display, DISPLAY_MODE_STARTUP);
			ui_clear_input(gui->ui, INPUT_INDEX_ENTER);
		}
		break;
	case DISPLAY_MODE_STARTUP:
		if (gui->ui->input_enter && gui->ui->input_debounce[INPUT_INDEX_ENTER] > INPUT_DEBOUNCE) {
			display_set_mode(gui->display, DISPLAY_MODE_OVERVIEW);
			ui_clear_input(gui->ui, INPUT_INDEX_ENTER);
		}
		break;
	default:
		break;
	}
	//ui_clear_inputs(gui->ui);
	display_prepare(gui->display);
	display_update(gui->display);
}
