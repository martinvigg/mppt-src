/*
 * timer.c
 *
 *  Created on: Nov 8, 2019
 *      Author: Martin Viggiano
 */

#include "timer.h"

void timer_init(timer_t1* timer, uint8_t ISR_period) {
	timer->ISR_flag = 0;
	timer->ISR_period = ISR_period;

	timer->tick_100ms.flag = 0;
	timer->tick_100ms.period = 100;
	timer->tick_100ms.tick = 0;

	timer->tick_250ms.flag = 0;
	timer->tick_250ms.period = 250;
	timer->tick_250ms.tick = 0;
}

void timer_update(timer_t1 *timer) {
	if (timer->tick_100ms.tick >= timer->tick_100ms.period) {
		timer->tick_100ms.tick = 0;
		timer->tick_100ms.flag = 1;
	}

	if (timer->tick_250ms.tick >= timer->tick_250ms.period) {
		timer->tick_250ms.tick = 0;
		timer->tick_250ms.flag = 1;
	}

	if (timer->tick_1000ms.tick >= timer->tick_1000ms.period) {
		timer->tick_1000ms.tick = 0;
		timer->tick_1000ms.flag = 1;
	}
}

void timer_isr(timer_t1* timer) {
	timer->tick_100ms.tick += timer->ISR_period;
	timer->tick_250ms.tick += timer->ISR_period;
	timer->tick_1000ms.tick += timer->ISR_period;
}
