/*
 * charge_param.h
 *
 *  Created on: Nov 8, 2019
 *      Author: Martin Viggiano
 */

#ifndef CHARGE_PARAM_H_
#define CHARGE_PARAM_H_

#include "charger.h"

extern const charge_param_t lead_acid_12V_600mA;

#endif /* CHARGE_PARAM_H_ */
