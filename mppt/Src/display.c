/*
 * display.c
 *
 *  Created on: Oct 8, 2019
 *      Author: Martin Viggiano
 */

#include "main.h"
#include "display.h"
#include "buck.h"
#include "charger.h"
#include "data_process.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

extern data_t data;
extern buck_t buck;
extern charge_t charge;

const char mode_text[10][8] = {"OFF", "STARTUP", "IDLE", "BULK", "ABS", "FLOAT", "E: OVP", "E: UVP", "E: TEMP", "ERROR"};
const char mppt_text[2][5] = {"", "MPPT"};
const char buck_text[4][2] = {"O", "i", "S", "A"};

static void _display_overview(display_t* display);
static void _display_overview_off(display_t *display);
static void _display_startup(display_t* display);
static void _display_hola(display_t* display);

void display_init(display_t* display, u8x8_msg_cb byte_cb, u8x8_msg_cb gpio_cb)
{
	display->byte_cb = byte_cb;
	display->gpio_cb = gpio_cb;

	u8g2_Setup_pcd8544_84x48_f(&(display->handle), U8G2_R0, display->byte_cb, display->gpio_cb);
	u8g2_InitDisplay(&(display->handle));
	u8g2_SetPowerSave(&(display->handle), 0);
	u8g2_SetFont(&(display->handle), u8g2_font_6x10_tf);
	display->mode = DISPLAY_MODE_OVERVIEW;
}

void display_set_mode(display_t* display, display_mode_t mode)
{
	display->mode = mode;
}

display_mode_t display_get_mode(display_t* display)
{
	return display->mode;
}

void display_update(display_t* display)
{
	u8g2_ClearBuffer(&(display->handle));
	u8g2_DrawStr(&(display->handle), 0, 8 , display->content[0]);
	u8g2_DrawStr(&(display->handle), 0, 16, display->content[1]);
	u8g2_DrawStr(&(display->handle), 0, 24, display->content[2]);
	u8g2_DrawStr(&(display->handle), 0, 32, display->content[3]);
	u8g2_DrawStr(&(display->handle), 0, 40, display->content[4]);
	u8g2_DrawStr(&(display->handle), 0, 48, display->content[5]);
	u8g2_SendBuffer(&(display->handle));
}

void display_prepare(display_t* display)
{
	switch (display->mode) {
	case DISPLAY_MODE_STARTUP:
		_display_startup(display);
		break;
	case DISPLAY_MODE_OVERVIEW:
		_display_overview(display);
		break;
	case DISPLAY_MODE_OVERVIEW_OFF:
			_display_overview_off(display);
			break;
	case DISPLAY_MODE_HOLA:
		_display_hola(display);
		break;
	default:
		break;
	}
}

/**
 *
 * @param display
 */
static void _display_overview_off(display_t *display) {
	float pv_v;
	float bat_v;

	pv_v = data.pv_v/1000.0;
	bat_v = data.bat_v/1000.0;

	sprintf(display->content[0], " PANEL BATERIA");
	sprintf(display->content[1], "%4.1f V %4.1f V", pv_v, bat_v);
	sprintf(display->content[2], "%5.2fA %5.2fA", 0.0, 0.0);
	sprintf(display->content[3], "%5.2fW %5.2fW", 0.0, 0.0);
	sprintf(display->content[5], "%-8s %s", mode_text[charge.charge_state], mppt_text[buck_get_mppt_enable(charge.buck)]);
	sprintf(display->content[4], "%1s:%.3d   E:%4.1f", buck_text[buck.mode], buck.duty, 0.0);
}

static void _display_overview(display_t *display) {
	float pv_v, pv_i, pv_w;
	float bat_v, bat_i, bat_w;
	float eff;

	pv_v = data.pv_v/1000.0;
	pv_i = data.pv_i/1000.0;
	pv_w = pv_v*pv_i;

	bat_v = data.bat_v/1000.0;
	bat_i = data.bat_i/1000.0;
	bat_w = bat_v*bat_i;

	if (pv_w != 0) {
		eff = bat_w / pv_w * 100;
	} else {
		eff = 0;
	}

	sprintf(display->content[0], " PANEL BATERIA");
	sprintf(display->content[1], "%4.1f V %4.1f V", pv_v, bat_v);
	sprintf(display->content[2], "%5.2fA %5.2fA", pv_i, bat_i);
	sprintf(display->content[3], "%5.2fW %5.2fW", pv_w, bat_w);
	sprintf(display->content[5], "%-8s %s", mode_text[charge.charge_state], mppt_text[buck_get_mppt_enable(charge.buck)]);
	sprintf(display->content[4], "%1s:%.3d   E:%4.1f", buck_text[buck.mode], buck.duty, eff);
}

static void _display_startup(display_t* display)
{
	sprintf(display->content[0], "MPPT");
	sprintf(display->content[1], "SOLAR CHARGER");
	sprintf(display->content[2], "v0.1");
	sprintf(display->content[3], "Martin");
	sprintf(display->content[4], "Viggiano");
	sprintf(display->content[5], " ");
}

static void _display_hola(display_t* display)
{
	sprintf(display->content[0], "hola");
	sprintf(display->content[1], "como");
	sprintf(display->content[2], "te");
	sprintf(display->content[3], "va");
	sprintf(display->content[4], "?");
	sprintf(display->content[5], " ");
}
