/*
 * ui.c
 *
 *  Created on: Oct 8, 2019
 *      Author: Martin Viggiano
 */

#include "main.h"
#include "ui.h"
#include "display.h"

#define READ_U1 HAL_GPIO_ReadPin(UI1_GPIO_Port, UI1_Pin)
#define READ_U2 HAL_GPIO_ReadPin(UI2_GPIO_Port, UI2_Pin)
#define READ_U3 HAL_GPIO_ReadPin(UI3_GPIO_Port, UI3_Pin)
#define READ_U4 HAL_GPIO_ReadPin(UI4_GPIO_Port, UI4_Pin)

#define INPUT_DEBOUNCE 20

extern display_t lcd;

void ui_clear_inputs(ui_t* ui) {
	ui->input_back = 0;
	ui->input_down = 0;
	ui->input_up = 0;
	ui->input_enter = 0;
}

void ui_clear_input(ui_t* ui, ui_input_index_t input) {
	switch (input) {
	case INPUT_INDEX_UP:
		ui->input_back = 0;
		ui->input_debounce[input] = 0;
		break;
	case INPUT_INDEX_DOWN:
		ui->input_down = 0;
		ui->input_debounce[input] = 0;
		break;
	case INPUT_INDEX_BACK:
		ui->input_back = 0;
		ui->input_debounce[input] = 0;
		break;
	case INPUT_INDEX_ENTER:
		ui->input_enter = 0;
		ui->input_debounce[input] = 0;
		break;
	default:
		break;
	}
}

void ui_init(ui_t* ui)
{
	ui->input_back = 0;
	ui->input_down = 0;
	ui->input_up = 0;
	ui->input_enter = 0;

	ui->input_debounce[INPUT_INDEX_UP] = 0;
	ui->input_debounce[INPUT_INDEX_DOWN] = 0;
	ui->input_debounce[INPUT_INDEX_BACK] = 0;
	ui->input_debounce[INPUT_INDEX_ENTER] = 0;
}

void ui_update_inputs(ui_t* ui)
{
	if (READ_U1) {
		ui->input_up = 1;
		ui->input_debounce[INPUT_INDEX_UP] += 2;
	} else if (ui->input_debounce[INPUT_INDEX_UP] < INPUT_DEBOUNCE) {
		ui->input_up = 0;
		ui->input_debounce[INPUT_INDEX_UP] = 0;
	}

	if (READ_U2) {
		ui->input_down = 1;
		ui->input_debounce[INPUT_INDEX_DOWN] += 2;
	} else if (ui->input_debounce[INPUT_INDEX_DOWN] < INPUT_DEBOUNCE) {
		ui->input_down = 0;
		ui->input_debounce[INPUT_INDEX_DOWN] = 0;
	}

	if (READ_U3) {
		ui->input_back = 1;
		ui->input_debounce[INPUT_INDEX_BACK] += 2;
	} else if (ui->input_debounce[INPUT_INDEX_BACK] < INPUT_DEBOUNCE) {
		ui->input_back = 0;
		ui->input_debounce[INPUT_INDEX_BACK] = 0;
	}

	if (READ_U4) {
		ui->input_enter = 1;
		ui->input_debounce[INPUT_INDEX_ENTER] += 2;
	} else if (ui->input_debounce[INPUT_INDEX_ENTER] < INPUT_DEBOUNCE) {
		ui->input_enter = 0;
		ui->input_debounce[INPUT_INDEX_ENTER] = 0;
	}
}


void ui_update(ui_t* ui)
{
	switch (display_get_mode(&lcd)) {
	case DISPLAY_MODE_STARTUP:
		if(ui->input_up) display_set_mode(&lcd, DISPLAY_MODE_OVERVIEW);
		break;
	case DISPLAY_MODE_OVERVIEW:
		if(ui->input_down) display_set_mode(&lcd, DISPLAY_MODE_STARTUP);
		break;
	case DISPLAY_MODE_HOLA:
	default:
		break;
	}
	ui_clear_inputs(ui);
}
