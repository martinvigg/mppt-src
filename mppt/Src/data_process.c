/*
 * data_process.c
 *
 *  Created on: Oct 8, 2019
 *      Author: Martin Viggiano
 */

#include "data_process.h"
#include "stm32f1xx_hal_adc.h"

#define DP_TEMP_INJ_RANK ADC_INJECTED_RANK_1
#define DP_VREF_INJ_RANK ADC_INJECTED_RANK_2

#define DP_GAIN_PV_V 2227/100 // Resistor divider: 100k & 4.7k
#define DP_GAIN_PV_I 10000/4545
#define DP_GAIN_BT_V 1320/100 // Resistor divider: 100k & 8.2k
#define DP_GAIN_BT_I 10000/4545
#define DP_GAIN_TEMP (1)
#define DP_OFFSET_PV_V 0
#define DP_OFFSET_BT_V 0
#define DP_OFFSET_PV_I 90
#define DP_OFFSET_BT_I 175
#define DP_OFFSET_TEMP 500
#define DP_VREF 2500

typedef enum {
	ADC_DMA_PV_V_INDEX = 0,
	ADC_DMA_BT_V_INDEX,
	ADC_DMA_PV_I_INDEX,
	ADC_DMA_BT_I_INDEX
} adc_dma_index_t;

void DP_Init(data_t* data, uint16_t* raw_data, uint16_t raw_data_length) {
	data->vref_den = 4096;
	data->vref_num = 3300;

	data->raw_data = raw_data;
	data->raw_data_length = raw_data_length;

	data->calibration[ADC_PV_V_INDEX].scale = DP_GAIN_PV_V;
	data->calibration[ADC_PV_V_INDEX].offset = DP_OFFSET_PV_V;

	data->calibration[ADC_BT_V_INDEX].scale = DP_GAIN_BT_V;
	data->calibration[ADC_BT_V_INDEX].offset = DP_OFFSET_BT_V;

	data->calibration[ADC_PV_I_INDEX].scale = DP_GAIN_PV_I;
	data->calibration[ADC_PV_I_INDEX].offset = DP_OFFSET_PV_I;

	data->calibration[ADC_BT_I_INDEX].scale = DP_GAIN_BT_I;
	data->calibration[ADC_BT_I_INDEX].offset = DP_OFFSET_BT_I;

	data->calibration[ADC_TEMP_INDEX].scale = DP_GAIN_TEMP;
	data->calibration[ADC_TEMP_INDEX].offset = DP_OFFSET_TEMP;

	data->bat_v = 0;
	data->bat_i = 0;
	data->pv_v  = 0;
	data->pv_i  = 0;
	data->tempC  = 0;
}

void DP_Average_data(data_t* data)
{
	uint16_t div = data->raw_data_length/4;
	uint32_t tmp[4];
	tmp[ADC_PV_V_INDEX] = 0;
	tmp[ADC_BT_V_INDEX] = 0;
	tmp[ADC_PV_I_INDEX] = 0;
	tmp[ADC_BT_I_INDEX] = 0;

	for (uint16_t i = 0; i < data->raw_data_length; i = i + 4) {
		tmp[0] += *(data->raw_data + i + ADC_DMA_PV_V_INDEX);
		tmp[1] += *(data->raw_data + i + ADC_DMA_BT_V_INDEX);
		tmp[2] += *(data->raw_data + i + ADC_DMA_PV_I_INDEX);
		tmp[3] += *(data->raw_data + i + ADC_DMA_BT_I_INDEX);
	}

	data->pv_v  = tmp[ADC_PV_V_INDEX]/div * data->vref_num / data->vref_den * DP_GAIN_PV_V - DP_OFFSET_PV_V;
	data->bat_v = tmp[ADC_BT_V_INDEX]/div * data->vref_num / data->vref_den * DP_GAIN_BT_V - DP_OFFSET_BT_V;

	data->pv_i  = tmp[ADC_PV_I_INDEX]/div * data->vref_num / data->vref_den * DP_GAIN_PV_I;
	if (data->pv_i > DP_OFFSET_PV_I) {
		data->pv_i -= DP_OFFSET_PV_I;
	} else {
		data->pv_i = 0;
	}
	data->bat_i = tmp[ADC_BT_I_INDEX]/div * data->vref_num / data->vref_den * DP_GAIN_BT_I;
	if (data->bat_i > DP_OFFSET_BT_I) {
		data->bat_i -= DP_OFFSET_BT_I;
	} else {
		data->bat_i = 0;
	}
}


void DP_Injected_data (data_t* data, ADC_HandleTypeDef* adc) {
	// Start injected conversion (temperaure and 2.5V reference)
	HAL_ADCEx_InjectedStart(adc);

	// Wait for conversion complete or 100ms timeout.
	while(HAL_ADCEx_InjectedPollForConversion(adc, 100));

	// Update temperature value
	data->tempC = (HAL_ADCEx_InjectedGetValue(adc, DP_TEMP_INJ_RANK) * data->vref_num / data->vref_den * DP_GAIN_TEMP - DP_OFFSET_TEMP);

	// Update vref num and den (for ADC counts to mV scaling).
	data->vref_den = HAL_ADCEx_InjectedGetValue(adc, DP_VREF_INJ_RANK);
	data->vref_num = DP_VREF;
}

void DP_Vref_Calibration (data_t* data, ADC_HandleTypeDef* adc) {
	uint16_t vref_sum = 0;

	for(uint8_t i = 0; i < 10; ++i) {
	// Start injected conversion (temperaure and 2.5V reference)
	HAL_ADCEx_InjectedStart(adc);

	// Wait for conversion complete or 100ms timeout.
	while(HAL_ADCEx_InjectedPollForConversion(adc, 100));
	vref_sum += HAL_ADCEx_InjectedGetValue(adc, DP_VREF_INJ_RANK);
	}

	// Update vref num and den (for ADC counts to mV scaling).
	data->vref_den = vref_sum/10;
	data->vref_num = DP_VREF;
}
